﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVD_Web_App.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            /*
            Project.DAL.Class1 cl = new Project.DAL.Class1();
            cl.test();
            */

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}