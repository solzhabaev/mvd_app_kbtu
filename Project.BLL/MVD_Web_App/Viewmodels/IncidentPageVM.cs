﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVD_Web_App.Viewmodels
{
    public class IncidentPageVM
    {
        public List<IncedentVM> Incidents { get; set; }
        public int Count { get; set; }
        public int Offset { get; set; }

        public IncidentPageVM() { }

        public IncidentPageVM(Models.AccidentPage pageData) 
        {
            this.Count = pageData.Count;
            this.Offset = pageData.PageNumber;
            this.Incidents = new List<IncedentVM>();
            foreach(var item in pageData.Incedents)
            {
                this.Incidents.Add(new IncedentVM(item));
            }
        }

    }
}