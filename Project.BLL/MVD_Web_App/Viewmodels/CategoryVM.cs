﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVD_Web_App.Viewmodels
{
    public class CategoryVM
    {
        public string Type { get; set; }
        public string Category { get; set; }
        public int CategoryId { get; set; }

        public CategoryVM() { }

        public CategoryVM(Models.Category categ) 
        {
            this.CategoryId = categ.ID;
            this.Category = categ.Name;
            this.Type = categ.TypeName;
        }

    }
}