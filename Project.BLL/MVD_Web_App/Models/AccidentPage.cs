﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVD_Web_App.Models
{
    public class AccidentPage
    {

        public ICollection<Accident> Incedents { get; set; }
        public int Count { get; set; }
        public int PageNumber { get; set; }

    }
}