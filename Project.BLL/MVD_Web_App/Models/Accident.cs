﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVD_Web_App.Models
{
    public class Accident
    {
        public long ID { get; set; }
        public DateTime Date { get; set; }
        public long CityID { get; set; }
        public string City { get; set; }
        public long RegionID { get; set; }
        public string Region { get; set; }
        public long StreetID { get; set; }
        public string Street { get; set; }
        public double Long { get; set; }
        public double Latit { get; set; }
        public string Description { get; set; }
        
    }
}