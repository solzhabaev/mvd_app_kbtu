//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVD_Web_App.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class RegionCadCodes
    {
        public RegionCadCodes()
        {
            this.Incidents = new HashSet<Incidents>();
        }
    
        public string CadCode { get; set; }
        public string CadTitle { get; set; }
        public int RegionID { get; set; }
    
        public virtual ICollection<Incidents> Incidents { get; set; }
        public virtual Regions Regions { get; set; }
    }
}
