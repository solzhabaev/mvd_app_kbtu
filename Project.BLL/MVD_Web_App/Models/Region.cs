﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVD_Web_App.Models
{
    public class Region
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
    }
}