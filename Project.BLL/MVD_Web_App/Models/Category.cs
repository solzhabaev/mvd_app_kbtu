﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVD_Web_App.Models
{
    public class Category
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string TypeName { get; set; }

    }
}