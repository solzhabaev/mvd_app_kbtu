﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _default : System.Web.UI.Page
    {
        private int countPostBack 
        {
            get { return Convert.ToInt32(this.ViewState["countPostBack"]); }
            set { this.ViewState["countPostBack"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(IsPostBack)
            {
                countPostBack++;

                lbl_postBackcount.Text = countPostBack.ToString();
            }
        }

        protected void btn_MakePostBack_Click(object sender, EventArgs e) 
        {

        }

    }
}