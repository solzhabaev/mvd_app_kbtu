﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVD_Web_App.Viewmodels
{
    public class RegionVM
    {
        public string City { get; set; }
        public string Region { get; set; }
        public int RegionId { get; set; }

        public RegionVM()
        {
        }

        public RegionVM(Models.Region region) 
        {
            this.City = region.City;
            this.Region = region.Name;
            this.RegionId = region.ID;
        }

    }
}