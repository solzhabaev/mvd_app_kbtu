﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OffViewState.aspx.cs" Inherits="WebApplication1.OffViewState" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button ID="btn_click" runat="server" OnClick="btn_click_Click" Text="go" Width="250" />
        <br />
        <asp:DropDownList ID="ddl_list" runat="server" EnableViewState="false" OnPreRender="ddl_list_PreRender" AutoPostBack="true" Width="250" />
    </div>
    </form>
</body>
</html>
