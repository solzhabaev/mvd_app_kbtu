﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVD_Web_App.Repositories
{
    public class IncedentRepo : IRepository<Models.AccidentPage>
    {
        public int LastID { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int CategoryID { get; set; }
        public int RowsLimit { get; set; }

        public ICollection<Models.AccidentPage> Get()
        {
            var res = new List<Models.AccidentPage>();

            using(var _context = new EF.SC_Context())
            {
                var accets = _context.Stud_Incedents_Get_List(LastID, DateFrom, DateTo, CategoryID, RowsLimit);
                var rowsMaxCount = _context.stud_Accident_Count_withProperty(LastID, DateFrom, DateTo, CategoryID, RowsLimit);
                
            }

            return res;
        }

        public Models.AccidentPage GetNew()
        {
            var res = new Models.AccidentPage();

            using (var _context = new EF.SC_Context())
            {
                var accets = _context.Stud_Incedents_Get_List(LastID, DateFrom, DateTo, CategoryID, RowsLimit);
                var rowsMaxCount = _context.stud_Accident_Count_withProperty(LastID, DateFrom, DateTo, CategoryID, RowsLimit);

                res.Count = rowsMaxCount.First().Value;

                res.Incedents = new List<Models.Accident>();

                foreach(var item in accets)
                {
                    res.Incedents.Add(new Models.Accident() 
                    {
                        ID = item.ID,
                        Date = item.IncidentTime,
                        City = item.CityTitle,
                        CityID = item.CityID.Value,
                        RegionID = item.CityRegionID.Value,
                        Region = item.CityRegionTitle,
                        StreetID = item.StreetLabelID.Value,
                        Street = item.StreetLabel,
                        Long = item.Longitude.HasValue ? item.Longitude.Value : 0,
                        Latit = item.Latitude.HasValue ? item.Latitude.Value : 0,
                        Description = item.EventDescr
                    });
                }

            }

            return res;
        }

        public Models.AccidentPage GetBy(int id)
        {
            var res = new Models.AccidentPage();

            using(var _context = new EF.SC_Context())
            {
                var currentItem = _context.Stud_Incedents_Get_ByID(id).First();

                var words = ConvertToList(currentItem.EventDescr);

                // todo. Data. if have '0' not right parse
                var regs = new List<long>() { 20202, 30202 };
                var categs = new List<long>() { 100027, 100070 };
                var fDate = new DateTime(2007, 11, 17);
                var lDate = new DateTime(2008, 10, 19);
                long limit = 100;
                var lastID = "2594560202";

                var sqlStr = sqlCommandGenerate(limit, lastID, fDate, lDate, regs, categs, words);

                var sameIncedents = _context.stud_IncidentsView.SqlQuery(sqlStr).ToList();

                res.Incedents = new List<Models.Accident>();

                foreach(var item in sameIncedents)
                {
                    res.Incedents.Add(new Models.Accident() 
                    {
                        ID = item.ID,
                        CityID = (int)item.CityID,
                        City = item.CityTitle,
                        Date = item.IncidentTime,
                        Description = item.EventDescr,
                        RegionID = (int)item.CityRegionID,
                        Region = item.CityRegionTitle,
                        StreetID = (int)item.StreetLabelID,
                        Street = item.StreetLabel,
                        Long = (int)item.Longitude,
                        Latit = (int)item.Latitude
                    });
                }

            }

            return res;
        }

        private List<string> ConvertToList(string str) 
        {
            var res = new List<string>();

            string buffer = "";

            for (int i = 0; i < str.Count(); i++)
            {
                var item = str[i];

                if (Char.IsWhiteSpace(item))
                {
                    res.Add(buffer);
                    buffer = "";
                }
                else
                {
                    buffer += Convert.ToString(item);
                }
            }

            res.RemoveAll(x => x == string.Empty);

            List<string> nItems = new List<string>();

            foreach(var word in res)
            {
                if(word.Count() > 5)
                {
                    nItems.Add(word.Remove(0, 2));
                    nItems.Add(word.Substring(0, word.Count() - 2));
                    nItems.Add(word.Remove(0, 1).Substring(0, word.Count() - 1));
                }
            }

            foreach(var item in nItems)
            {
                res.Add(item);
            }

            return res;

        }


        private string sqlCommandGenerate(long limitRows, string lastID, DateTime dateFrom, DateTime dateTo, List<long> regions, List<long> categories, List<string> words) 
        {
            string regs = string.Empty;
            foreach(var item in regions)
            {
                if (regs == string.Empty)
                    regs += string.Format("{0} = {1}", "CityRegionID", item);
                else
                    regs += string.Format(" OR {0} = {1}", "CityRegionID", item);
            }
            regs = string.Format("{0}", regs);

            string categs = string.Empty;
            foreach(var item in categories)
            {
                if (categs == string.Empty)
                    categs += string.Format("{0} = {1}", "CategoryID", item);
                else
                    categs += string.Format(" OR {0} = {1}", "CategoryID", item);
            }
            categs = string.Format("{0}", categs);

            string wrds = string.Empty;
            foreach(var item in words)
            {
                if (wrds == string.Empty)
                    wrds += string.Format("{0} LIKE '%{1}%'", "EventDescr", item);
                else
                    wrds += string.Format(" OR {0} LIKE '%{1}%'", "EventDescr", item);
            }
            wrds = string.Format("{0}", wrds);

            string sqlCommand = string.Format(  @"SELECT TOP({0}) * FROM stud_Incedent " + 
                                                "WHERE ID > {1} AND " + 
                                                "('{2}' < IncidentTime AND IncidentTime < '{3}') AND " + 
                                                "({4}) AND " + 
                                                "({5}) AND " +
                                                "({6})", 
                                                limitRows,
                                                lastID,
                                                string.Format("{0}{1}{2}", dateFrom.Year, dateFrom.Month, dateFrom.Day),
                                                string.Format("{0}{1}{2}", dateTo.Year, dateTo.Month, dateTo.Day),
                                                regs,
                                                categs, 
                                                wrds);

            /*
             *  SELECT TOP(100) * FROM stud_Incedent
                WHERE 
	            ID > 2594560202 AND
	            ('20071107' < IncidentTime  AND IncidentTime < '20081009') AND
	            (CityRegionID = 20202 OR CityRegionID = 30202) AND
	            (CategoryID = 100027 OR CategoryID = 100070) AND
	            (EventDescr LIKE '%ЗБИТ%' OR EventDescr LIKE '%ЧИНГИЗ%')

             */

            return sqlCommand;
        }


    
    }
}