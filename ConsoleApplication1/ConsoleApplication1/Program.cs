﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(ConsoleApplication1.StaticClass.Count);
            Console.WriteLine("1 - - - - - ");
            Console.ReadKey();

            Console.WriteLine(ConsoleApplication1.StaticClass.Count);
            Console.WriteLine("2 - - - - - ");
            Console.ReadKey();

            Console.WriteLine(ConsoleApplication1.StaticClass.Count);
            Console.WriteLine("3 - - - - - ");
            Console.ReadKey();

            Console.WriteLine(ConsoleApplication1.StaticClass.Count);
            Console.WriteLine("4 - - - - - ");
            Console.ReadKey();

            Console.WriteLine(ConsoleApplication1.StaticClass.Count);
            Console.WriteLine("last - - - - - ");
            Console.ReadKey();

            Console.WriteLine(ConsoleApplication1.StaticClass.Count);
            Console.WriteLine("off - - - - - ");
            Console.ReadKey();
        }
    }
}
