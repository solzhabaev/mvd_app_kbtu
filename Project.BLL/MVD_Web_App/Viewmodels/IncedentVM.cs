﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVD_Web_App.Viewmodels
{
    public class IncedentVM
    {
        public int IncedentId { get; set; }
        public DateTime Date { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Description { get; set; }

        public IncedentVM() 
        {
        }

        public IncedentVM(Models.Accident accident) 
        {
            this.IncedentId = (int)accident.ID;
            this.Date = accident.Date;
            this.City = accident.City;
            this.Street = accident.Street;
            this.Description = accident.Description;
        }
    }
}