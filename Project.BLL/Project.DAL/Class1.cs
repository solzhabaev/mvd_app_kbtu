﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.DAL
{
    public class Class1
    {
        public void test() 
        {
            /*
            using(var context = new EF.SCEntities())
            {
                var resView = context.stud_IncidentsView;

                var resRegions = context.Stud_Region_Get();

                var resCategories = context.Stud_Categories_Get();

            }
            */

            using(var _context = new EF.SCEntities1())
            {
                var resView = _context.stud_IncidentsView;
                var resRegions = _context.Stud_Region_Get();
                var resCateg = _context.Stud_Categories_Get();
            }

        }
    }
}
