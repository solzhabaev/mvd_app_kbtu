﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public static class StaticClass
    {
        private static int _count;
        public static int Count 
        {
            get 
            {
                _count++;
                return _count;
            }
        }
    }
}
