//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVD_Web_App.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class CardStatuses
    {
        public CardStatuses()
        {
            this.Incidents = new HashSet<Incidents>();
            this.IncidentsManual = new HashSet<IncidentsManual>();
        }
    
        public int ID { get; set; }
        public string Title { get; set; }
        public int StatusGroupID { get; set; }
    
        public virtual CardStatusGroups CardStatusGroups { get; set; }
        public virtual ICollection<Incidents> Incidents { get; set; }
        public virtual ICollection<IncidentsManual> IncidentsManual { get; set; }
    }
}
