﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class OffViewState : System.Web.UI.Page
    {
        private List<string> _list 
        {
            get 
            {
                return new List<string>() 
                {
                    "item1", "item2", "item3", "item4", "item5", "item6", "item7", "item8", "item9", "item10"
                };
            }
        }

        protected override void OnInit(EventArgs e)
        {
            //base.OnInit(e);
            var val = ddl_list.SelectedValue;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            /*
            if (!IsPostBack) 
            {
                
            }*/
            ddl_list.Items.Clear();
            foreach (var item in _list)
            {
                ddl_list.Items.Add(new ListItem(item));
            }
        }

        protected void btn_click_Click(object sender, EventArgs e)
        {
            
            
        }

        protected void ddl_list_PreRender(object sender, EventArgs e) 
        {
            /*
            var currentItem = sender as DropDownList;
            var val = currentItem.SelectedValue;
            */
        }

    }
}