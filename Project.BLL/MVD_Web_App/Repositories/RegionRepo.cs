﻿using MVD_Web_App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVD_Web_App.Repositories
{
    public class RegionRepo : IRepository<Models.Region>
    {

        public ICollection<Models.Region> Get()
        {
            var res = new List<Models.Region>();

            using (var _context = new EF.SC_Context())
            {
                var regions = _context.Stud_Region_Get();

                foreach(var item in regions)
                {
                    res.Add(new Models.Region() 
                    {
                        ID = item.ID,
                        Name = item.Title,
                        City = item.City
                    });
                }
            }
            return res;
        }

        public Models.Region GetBy(int id)
        {
            throw new NotImplementedException();
        }
    }
}