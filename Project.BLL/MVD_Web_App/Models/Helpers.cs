﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVD_Web_App.Models
{
    public class Helpers
    {
        public static List<Viewmodels.RegionVM> toListRegionVM(ICollection<Models.Region> regions) 
        {
            var res = new List<Viewmodels.RegionVM>(); ;

            foreach(var item in regions)
            {
                res.Add(new Viewmodels.RegionVM(item));
            }

            return res;
        }

        public static List<Viewmodels.CategoryVM> toListCategoryVM(ICollection<Models.Category> categories)
        {
            var res = new List<Viewmodels.CategoryVM>(); ;

            foreach (var item in categories)
            {
                res.Add(new Viewmodels.CategoryVM(item));
            }

            return res;
        }

    }
}