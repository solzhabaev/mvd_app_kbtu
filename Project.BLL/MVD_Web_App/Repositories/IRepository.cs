﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVD_Web_App.Repositories
{
    interface IRepository<T>
    {
        ICollection<T> Get();

        T GetBy(int id);

    }
}
