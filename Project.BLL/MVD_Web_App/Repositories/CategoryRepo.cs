﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVD_Web_App.Repositories
{
    public class CategoryRepo : IRepository<Models.Category>
    {
        public ICollection<Models.Category> Get()
        {
            var res = new List<Models.Category>();

            using (var _context = new EF.SC_Context())
            {
                var categ = _context.Stud_Categories_Get();

                foreach(var item in categ)
                {
                    res.Add(new Models.Category() 
                    {
                        ID = item.ID,
                        Name = item.Title,
                        TypeName = item.CategType
                    });
                }

            }

            return res;
        }

        public Models.Category GetBy(int id)
        {
            throw new NotImplementedException();
        }
    }
}